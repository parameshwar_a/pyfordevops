base_name = input("Enter the base name: ")
num_of_machine = int(input("Enter the number of machines: "))
port_starting = 8080
image_name = input("Enter the image name: ")


with open("docker_provision.sh", "a") as my_script:
    my_script.write("#!/bin/bash")
    my_script.write("\n")
    for iter in range(num_of_machine):
        my_script.write(f"docker run -d --name {base_name+"_"+str(iter)} -p {port_starting+iter}:80 {image_name}")
        my_script.write("\n")
