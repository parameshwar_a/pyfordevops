import requests

search_string = input("Enter the string you want to search: ")

data = requests.get(f"https://api.chucknorris.io/jokes/search?query={search_string}")

print(data.json()["result"][0]['url'])
