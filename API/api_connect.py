import requests 
import json 

#print(dir(requests))

search_string = input("Enter the word you want to search")
data = requests.get(f"https://api.dictionaryapi.dev/api/v2/entries/en/{search_string}")

#print(type(data), dir(data))

myjson = data.json()
mystring = json.dumps(myjson, indent=2)
print(mystring)

