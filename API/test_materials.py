import json

mydata={"2023":{"dada":8, "parking":7, "jailer":6},2024:{"captain_miller":5, "ayalaan":3, "singapore_saloon":6}}

print(json.dumps(mydata, indent=4))


food_db = { "Indian":{"south":{"Breakfast":"idly_sambar", "Lunch": "meals", "Dinner": "Dosa_sambar"}, "north":{"Breakfast":"Poha", "Lunch":"Thaali", "Dinner":"Chappathi_panner_butter_masala"}},"Italian":{"Breakfast":"Cloud_Eggs", "Lunch":"Chickpea_soup_and_Quiche"},"American":{"Breakfast":"hotdog", "dinner":"mac_and_cheese"}}
print(json.dumps(food_db, indent=4))
