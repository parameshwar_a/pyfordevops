port_starting = 8080
with open("requirement.txt", "r") as myinput:
    datalines = myinput.readlines()

with open("docker_provision.sh", "a") as my_script:
    my_script.write("#!/bin/bash")
    my_script.write("\n")
    for line in datalines:
        line_data = line.replace("\n", "")
        image_name,base_name,num_of_machine = line_data.split()
        for iter in range(int(num_of_machine)):
            my_script.write(f"docker run -d --name {base_name+'_'+str(iter)} -p {port_starting}:80 {image_name}")
            my_script.write("\n")
            port_starting=port_starting+1
